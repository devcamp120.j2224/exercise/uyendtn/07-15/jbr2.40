public class App {
    public static void main(String[] args) throws Exception {
        //5. tạo 2 person và in ra systemout
        Person person1 = new Person("john", "US");
        Person person2 = new Person("Mary", "UK");
        System.out.println(person1.toString());
        System.out.println(person2.toString());
        //6. tạo 2 student và in ra systemout
        Student student1 = new Student("John", "US", "IT", 2022, 200000);
        Student student2 = new Student("Mary", "UK", "Economic", 2022, 150000);
        System.out.println(student1.toString());
        System.out.println(student2.toString());
        //7. tạo 2 staff và in ra systemout
        Staff staff1 = new Staff("dave", "US", "UBC", 100000);
        Staff staff2 = new Staff("An", "VN", "UEH", 150000);
        System.out.println(staff1.toString());
        System.out.println(staff2.toString());
    }
}
