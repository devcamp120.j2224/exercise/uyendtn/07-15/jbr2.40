public class Person {
    String name;
    public String getName() {
        return name;
    }
    String address;
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public Person() {
    }
    public Person(String name, String address) {
        this.name = name;
        this.address = address;
    }
    public String toString() {
        return "Person[name= " + name + ", address= " + address + "]";
    }

}
